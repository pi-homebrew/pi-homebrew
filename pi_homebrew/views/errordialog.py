from gi.repository import Gtk

from logdecorator import log_on_end
import logging


class ErrorDialog(Gtk.MessageDialog):
    @log_on_end(logging.INFO, 'Error dialog initialized.')
    def __init__(self, title: str, text: str, **kwargs) -> None:
        kwargs['message_type'] = Gtk.MessageType.ERROR
        kwargs['title'] = title
        kwargs['text'] = text
        kwargs['modal'] = kwargs.get('modal', True)
        Gtk.MessageDialog.__init__(self, **kwargs)
        self.add_button('Ok', Gtk.ResponseType.OK)
