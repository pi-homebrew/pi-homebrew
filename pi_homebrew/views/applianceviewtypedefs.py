import dataclasses

from pi_homebrew.controllers.appliancetypedefs import appliancetypedefs
from pi_homebrew.views.dataclasses import ApplianceViewTypeDef

applianceviewtypedefs = tuple(
    ApplianceViewTypeDef(*dataclasses.astuple(appliancetypedef))
    for appliancetypedef in appliancetypedefs)
