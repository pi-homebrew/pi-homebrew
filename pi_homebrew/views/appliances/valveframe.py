from typing import TYPE_CHECKING

from gi.repository import Gtk
from pi_homebrew.views.appliances import ApplianceFrame

if TYPE_CHECKING:
    from pi_homebrew.views import MainWindow


class ValveFrame(ApplianceFrame):
    def __init__(self, parent: 'MainWindow') -> None:
        super().__init__(parent)
