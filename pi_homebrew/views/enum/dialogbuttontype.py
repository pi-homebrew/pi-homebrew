from enum import IntFlag


class DialogButtonType(IntFlag):
    NONE = 0
    OK = 1
    YES = 2
    ACCEPT = 4
    NO = 8
    CANCEL = 16
    CLOSE = 32
    RETRY = 64
    SAVE = 128
    DONT_SAVE = 256
