from typing import TYPE_CHECKING

from gi.repository import Gtk

from pi_homebrew.views import BrewHousePage
from pi_homebrew.models.enum import TabType

import logging
from logdecorator import log_on_start, log_on_end

if TYPE_CHECKING:
    from pi_homebrew.views import MainWindow
    from pi_homebrew.pihomebrewapp import PiHomebrewApp


class AppliancesNotebook(Gtk.Notebook):
    @log_on_end(logging.INFO, 'Appliances notebook initialized')
    def __init__(self, application: 'PiHomebrewApp', parent: 'MainWindow'):
        Gtk.Notebook.__init__(self)

        self.parent = parent
        self.application = application

        self.brew_house_page = BrewHousePage(self.parent)
        self.append_page(self.brew_house_page, Gtk.Label.new('Brew house'))

    @log_on_start(logging.INFO, 'Refresh notebook appliances')
    def refresh_appliances(self) -> None:
        self.brew_house_page.refresh_appliances([
            appliance for appliance in self.application.appliances
            if appliance.on_tab == TabType.BREW_HOUSE
        ])
