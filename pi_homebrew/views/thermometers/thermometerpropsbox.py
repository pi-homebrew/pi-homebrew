import logging
from concurrent import futures
from typing import TYPE_CHECKING
from typing import Dict
from typing import Optional
from typing import Tuple
from typing import Union

from gi.repository import GLib
from gi.repository import Gtk
from gi.repository import Pango

from logdecorator import log_on_end
from logdecorator import log_exception
from logdecorator import log_on_start
from owclient.devices import Thermometer as ThermometerDevice

from pi_homebrew.models import Thermometer as ThermometerModel
from pi_homebrew.views.dataclasses.widgeteventhandler import WidgetEventHandler
from pi_homebrew.controllers import thermometercontroller

if TYPE_CHECKING:
    from pi_homebrew.pihomebrewapp import PiHomebrewApp
    from pi_homebrew.views import ApplianceThermometersBox


class ThermometerPropsBox(Gtk.Box):
    @log_on_end(logging.INFO, 'ThermometerPropsBox initialized')
    def __init__(self,
                 application: 'PiHomebrewApp',
                 parent: 'ApplianceThermometersBox') -> None:
        Gtk.Box.__init__(self)

        self.application = application
        self.parent = parent
        self.devices: Dict[str, Dict[str, Union[int, str]]] = dict()
        self.thermometer: Optional[ThermometerModel] = None

        controls_grid = Gtk.Grid(column_homogeneous=True,
                                 column_spacing=5,
                                 row_spacing=5,
                                 border_width=5)
        self.add(controls_grid)

        name_label = Gtk.Label('Name:')
        name_label.props.xalign = 0
        controls_grid.attach(name_label, 0, 0, 1, 1)

        self.name_entry = Gtk.Entry(max_length=50)
        self.name_entry.set_icon_from_icon_name(
            Gtk.EntryIconPosition.SECONDARY,
            'emblem-important-symbolic')

        controls_grid.attach(self.name_entry, 0, 1, 1, 1)

        device_label = Gtk.Label('Associated device:')
        device_label.props.xalign = 0
        controls_grid.attach(device_label, 0, 2, 1, 1)

        self.devices_store = Gtk.ListStore(str, str)
        loading_iter = self.devices_store.append([None, 'Loading...'])
        self.devices_combo = Gtk.ComboBox.new_with_model(self.devices_store)
        self.devices_combo.set_active_iter(loading_iter)

        device_renderer = Gtk.CellRendererText()
        self.devices_combo.pack_start(device_renderer, True)
        self.devices_combo.add_attribute(device_renderer, 'text', 1)
        self.devices_combo.set_id_column(0)
        controls_grid.attach(self.devices_combo, 0, 3, 1, 1)

        description_label = Gtk.Label('Description:')
        description_label.props.xalign = 0
        controls_grid.attach(description_label, 1, 0, 1, 1)

        description_scroll_window = Gtk.ScrolledWindow(
            hscrollbar_policy=Gtk.PolicyType.AUTOMATIC,
            vscrollbar_policy=Gtk.PolicyType.AUTOMATIC,
            shadow_type=Gtk.ShadowType.IN)
        self.description_text_buffer = Gtk.TextBuffer()
        description_text_view = Gtk.TextView(
            buffer=self.description_text_buffer,
            monospace=True,
            wrap_mode=Gtk.WrapMode.WORD,
            hexpand=True)
        description_text_view.props.monospace = True
        description_text_view.props.wrap_mode = Pango.WrapMode.WORD
        description_scroll_window.add(description_text_view)
        controls_grid.attach(description_scroll_window, 1, 1, 1, 3)

        self.handlers = (WidgetEventHandler(self.name_entry,
                                            'changed',
                                            self.name_entry_on_changed),
                         WidgetEventHandler(self.devices_combo,
                                            'changed',
                                            self.devices_combo_on_changed))

    @log_on_start(logging.INFO, 'Devices combo changed')
    def devices_combo_on_changed(self, combo: Gtk.ComboBox) -> None:
        if self.thermometer is not None:
            active_iter = combo.get_active_iter()

            if active_iter is not None:
                device_path = combo.get_model().get_value(active_iter, 0)
                self.thermometer.path = device_path
                self.parent.thermometer_on_change(self.thermometer)

    @log_on_start(logging.INFO, 'Devices readed.')
    @log_exception(logging.ERROR,
                   'Error loading devices',
                   on_exceptions=(Exception,
                                  ))
    def devices_on_readed(self, devices_future: futures.Future) -> None:
        result = devices_future.result()
        devices = {
            path: {
                'id': device.id,
                'type': type(device).__name__
            }
            for (path,
                 device) in result.items()
        }
        GLib.idle_add(self.load_devices_in_combo, devices)

    @log_on_start(logging.INFO, 'Load devices in combo: <{devices!r}>.')
    def load_devices_in_combo(self,
                              devices: Dict[str,
                                            Dict[str,
                                                 str]]) -> None:
        self.devices_store.clear()

        used_paths = tuple(used_path for used_path in self.parent.used_paths
                           if used_path != self.thermometer.path)

        path: str
        device: Dict[str, ThermometerDevice]
        for (path, device) in devices.items():
            if path not in used_paths:
                self.devices_store.append(
                    [path,
                     f"{device['type']} - {device['id']}"])

        self.devices_combo.set_sensitive(len(self.devices_store) > 0)

    @log_on_start(logging.INFO, 'Name entry changed <{entry.props.name}>.')
    def name_entry_on_changed(self, entry: Gtk.Entry) -> None:
        if self.thermometer is not None:
            name = entry.get_text().strip()
            self.thermometer.name = name

            self.__update_name_entry_icon(entry)
            self.__update_name_entry_tooltip(entry)

            self.parent.thermometer_on_change(self.thermometer)

    @log_on_start(logging.INFO, 'Update name entry icon.')
    def __update_name_entry_icon(self,
                                 entry: Optional[Gtk.Entry] = None) -> None:
        if self.thermometer is not None:
            if entry is None:
                entry = self.name_entry

            if self.thermometer.name == '':
                entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY,
                                              'emblem-important-symbolic')
            else:
                if thermometercontroller.is_unique_name(
                        self.application.db_session,
                        self.thermometer):
                    entry.set_icon_from_icon_name(
                        Gtk.EntryIconPosition.SECONDARY,
                        'emblem-ok-symbolic')
                else:
                    entry.set_icon_from_icon_name(
                        Gtk.EntryIconPosition.SECONDARY,
                        'emblem-important-symbolic')

    @log_on_start(logging.INFO, 'Update name entry tooltip.')
    def __update_name_entry_tooltip(self,
                                    entry: Optional[Gtk.Entry] = None) -> None:
        if self.thermometer is not None:
            if entry is None:
                entry = self.name_entry

            if self.thermometer.name == '':
                entry.set_tooltip_text('The thermometer must have a name')
            else:
                if thermometercontroller.is_unique_name(
                        self.application.db_session,
                        self.thermometer) is True:
                    entry.set_tooltip_text(None)
                else:
                    entry.set_tooltip_text(
                        f'The name {self.thermometer.name} is already used.')

    @log_on_start(logging.INFO, 'Disconnect widget handlers.')
    def disconnect_handlers(
            self,
            filter_widgets: Optional[Tuple[Gtk.Widget,
                                           ...]] = None) -> None:
        handlers = self.handlers

        if filter_widgets is not None:
            handlers = tuple(handler for handler in handlers
                             if handler.widget in filter_widgets)

        for handler in handlers:
            if handler.handler_id is not None:
                handler.widget.disconnect(handler.handler_id)
                handler.handler_id = None

    @log_on_start(logging.INFO, 'Connect widget handlers')
    def connect_handlers(
            self,
            filter_widgets: Optional[Tuple[Gtk.Widget,
                                           ...]] = None) -> None:
        handlers = self.handlers

        if filter_widgets is not None:
            handlers = tuple(handler for handler in handlers
                             if handler.widget in filter_widgets)

        for handler in handlers:
            handler.handler_id = handler.widget.connect(
                handler.event_name,
                handler.handler_func)

    @log_on_start(logging.INFO, 'Set thermometer <{thermometer!r}>.')
    def set_thermometer(self, thermometer: ThermometerModel) -> None:
        self.disconnect_handlers()

        devices_future = thermometercontroller.load_thermometer_devices()
        devices_future.add_done_callback(self.devices_on_readed)

        self.thermometer = thermometer

        if self.thermometer.name is not None:
            self.name_entry.set_text(self.thermometer.name)

        self.__update_name_entry_icon()
        self.__update_name_entry_tooltip()

        if self.thermometer.description is not None:
            self.description_text_buffer.set_text(self.thermometer.description)

        def set_combo_active_id(future: futures.Future) -> None:
            GLib.idle_add(self.set_path_on_combo)

        devices_future.add_done_callback(set_combo_active_id)
        self.connect_handlers((self.name_entry, ))

    @log_on_start(logging.INFO, 'Set path on combo')
    def set_path_on_combo(self) -> None:
        self.devices_combo.set_active_id(self.thermometer.path)
        self.connect_handlers((self.devices_combo, ))

    @log_on_start(logging.INFO, 'Clear thermometer props box.')
    def clear(self) -> None:
        thermometercontroller.refresh_thermometer(self.application.db_session,
                                                  self.thermometer)
        self.thermometer = None
        self.name_entry.set_text('')
        self.description_text_buffer.set_text('')
        self.devices_combo.set_active_iter(None)
