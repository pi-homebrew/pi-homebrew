import logging
from typing import TYPE_CHECKING
from typing import List

import inflect
from gi.repository import Gtk
from logdecorator import log_on_start

from pi_homebrew.controllers.appliancetypedefs import appliancetypedefs
from pi_homebrew.models import Appliance
from pi_homebrew.models.enum import ApplianceType
from pi_homebrew.views.appliances import KettleFrame

if TYPE_CHECKING:
    from pi_homebrew.views import MainWindow


class BrewHousePage(Gtk.Box):
    @log_on_start(logging.INFO, 'Initializing BrewHousePage')
    def __init__(self, parent: 'MainWindow') -> None:
        Gtk.Box.__init__(self)

        self.parent = parent
        self.inflect = inflect.engine()

        self.set_orientation(Gtk.Orientation.VERTICAL)

    @log_on_start(logging.INFO, 'Clear appliance type frames')
    def clear_appliance_type_frames(self) -> None:
        for frame in self.get_children():
            frame.destroy()

    @log_on_start(logging.INFO, 'Refresh appliances')
    def refresh_appliances(self, appliances: List[Appliance]) -> None:
        self.clear_appliance_type_frames()

        kettles = [
            appliance for appliance in appliances
            if appliance.appliance_type == ApplianceType.KETTLE
        ]

        if len(kettles) > 0:
            kettles_frame = Gtk.Frame(label=self.inflect.plural(
                appliancetypedefs[ApplianceType.KETTLE].name).capitalize(),
                                      label_xalign=0.5,
                                      border_width=8)

            self.pack_start(kettles_frame, True, True, 0)

            kettles_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL,
                                  homogeneous=True,
                                  spacing=8,
                                  border_width=8)
            kettles_frame.add(kettles_box)

            for kettle in kettles:
                kettle_view = KettleFrame(self.parent)
                kettle_view.set_appliance(kettle)
                kettles_box.pack_start(kettle_view, True, True, 0)

        self.show_all()
