import logging
from typing import TYPE_CHECKING
from typing import Optional

from gi.repository import Gtk
from logdecorator import log_exception
from logdecorator import log_on_end
from logdecorator import log_on_start

from pi_homebrew.controllers import actuatorcontroller
from pi_homebrew.models import Actuator
from pi_homebrew.models.enum import ControlType
from pi_homebrew.views.editors.actuatorpropsbox import ActuatorPropsBox
from pi_homebrew.views.editors.editappliancebox import EditApplianceBox

if TYPE_CHECKING:
    from pi_homebrew.pihomebrewapp import PiHomebrewApp
    from pi_homebrew.views import PanedConfig


class EditActuatorBox(EditApplianceBox):
    @log_on_start(logging.INFO, 'Initializing EditActuatorBox.')
    def __init__(self,
                 application: 'PiHomebrewApp',
                 parent: 'PanedConfig') -> None:
        super().__init__(application, parent)

        self.appliance: Optional[Actuator] = None
        self.props_box = ActuatorPropsBox(application, self)
        self.notebook.insert_page(self.props_box,
                                  Gtk.Label.new('Properties'),
                                  0)

    @log_on_start(logging.INFO, 'Save appliance {self.appliance!r}.')
    @log_on_end(logging.INFO, 'Appliance saved {self.appliance!r}.')
    @log_exception(logging.ERROR, 'Error saving apliance {self.appliance!r}.')
    def save_appliance(self) -> Optional[Actuator]:
        if self.appliance is not None:
            self.appliance = actuatorcontroller.save_actuator(
                self.application.db_session,
                self.appliance)
            self.parent.appliance_on_change(self.appliance)
            return self.appliance

    def set_appliance(self, actuator: Actuator) -> None:
        if actuator.id is None:
            actuator.control_type = ControlType.ON_OFF.value
            actuator.max_value = 0
            actuator.min_value = 0

        super().set_appliance(actuator)
