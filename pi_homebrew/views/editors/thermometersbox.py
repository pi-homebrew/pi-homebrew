import logging
from typing import TYPE_CHECKING
from typing import Optional
from typing import Tuple

from gi.repository import Gtk
from logdecorator import log_on_end
from logdecorator import log_on_start

from pi_homebrew.controllers import thermometercontroller
from pi_homebrew.models import Appliance
from pi_homebrew.models import Thermometer
from pi_homebrew.views import ConfirmDialog
from pi_homebrew.views.enum import DialogButtonType
from pi_homebrew.views.thermometers import ThermometerPropsBox

if TYPE_CHECKING:
    from pi_homebrew.views.editors.editappliancebox import EditApplianceBox
    from pi_homebrew.pihomebrewapp import PiHomebrewApp


class ThermometersBox(Gtk.Box):
    @log_on_start(logging.INFO, 'Initializing ThermometersBox')
    def __init__(self,
                 application: 'PiHomebrewApp',
                 parent: 'EditApplianceBox') -> None:
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL, spacing=8)

        self.application = application
        self.parent = parent

        self.appliance: Optional[Appliance] = None
        self.thermometer: Optional[Thermometer] = None
        self.selected_iter: Optional[Gtk.TreeIter] = None
        self.used_paths: Tuple[str, ...] = tuple()

        toolbar = Gtk.Toolbar(show_arrow=True,
                              toolbar_style=Gtk.ToolbarStyle.ICONS,
                              icon_size=Gtk.IconSize.BUTTON)
        self.pack_start(toolbar, False, False, 0)

        self.add_button = Gtk.ToolButton(icon_name='document-new-symbolic',
                                         tooltip_text='Add thermometer')
        self.add_button.connect('clicked', self.add_button_on_clicked)
        toolbar.add(self.add_button)

        self.delete_button = Gtk.ToolButton(sensitive=False,
                                            icon_name='edit-delete-symbolic',
                                            tooltip_text='Delete thermometer')
        self.delete_button.connect('clicked', self.delete_button_on_clicked)
        toolbar.add(self.delete_button)

        self.props_box = ThermometerPropsBox(self.application, self)
        self.pack_start(self.props_box, False, False, 0)

        self.props_control_box = Gtk.ButtonBox(
            orientation=Gtk.Orientation.HORIZONTAL,
            layout_style=Gtk.ButtonBoxStyle.END,
            spacing=8,
            border_width=8)
        self.pack_start(self.props_control_box, False, False, 0)

        add_icon = Gtk.Image.new_from_icon_name('list-add-symbolic',
                                                Gtk.IconSize.BUTTON)
        self.save_thermometer_button = Gtk.Button(always_show_image=True,
                                                  image=add_icon,
                                                  label='Save',
                                                  sensitive=False)
        self.save_thermometer_button.connect(
            'clicked',
            self.save_thermometer_button_on_clicked)
        self.props_control_box.pack_start(self.save_thermometer_button,
                                          False,
                                          False,
                                          0)

        revert_icon = Gtk.Image.new_from_icon_name('document-revert-symbolic',
                                                   Gtk.IconSize.BUTTON)
        self.cancel_thermometer_button = Gtk.Button(always_show_image=True,
                                                    image=revert_icon,
                                                    label='Cancel')
        self.cancel_thermometer_button.connect(
            'clicked',
            self.cancel_thermometer_button_on_clicked)
        self.props_control_box.pack_start(self.cancel_thermometer_button,
                                          False,
                                          False,
                                          0)

        self.thermometers_store = Gtk.ListStore(int, str, str)
        thermometers_list_view = Gtk.TreeView(
            model=self.thermometers_store,
            enable_grid_lines=Gtk.TreeViewGridLines.HORIZONTAL)

        name_renderer = Gtk.CellRendererText()
        name_column = Gtk.TreeViewColumn('Name', name_renderer, text=1)
        name_column.set_expand(True)
        thermometers_list_view.append_column(name_column)

        path_renderer = Gtk.CellRendererText()
        path_column = Gtk.TreeViewColumn('Associated device',
                                         path_renderer,
                                         text=2)
        path_column.set_expand(True)
        thermometers_list_view.append_column(path_column)

        self.thermometers_selection = thermometers_list_view.get_selection()
        self.thermometers_selection.connect(
            'changed',
            self.thermometers_selection_on_changed)
        self.thermometers_selection.set_select_function(
            self.can_change_selection,
            None)

        scrollable_thermometers_list = Gtk.ScrolledWindow(vexpand=True)
        scrollable_thermometers_list.add(thermometers_list_view)
        self.pack_start(scrollable_thermometers_list, True, True, 0)

    @log_on_start(logging.INFO,
                  'Set thermometers box appliance {appliance!r}.')
    def set_appliance(self, appliance: Appliance) -> None:
        self.appliance = appliance
        self.update_thermometers_store()
        self.update_used_paths()
        self.update_props_visibility()

    @log_on_start(logging.INFO, 'Clear thermometers store.')
    def clear_thermometers_store(self) -> None:
        self.thermometers_store.clear()

    @log_on_start(logging.INFO, 'Update thermometers store.')
    def update_thermometers_store(self) -> None:
        self.clear_thermometers_store()

        if self.appliance is not None:
            for thermometer in self.appliance.thermometers:  # type:ignore [attr-defined] # noqa F821
                self.thermometers_store.append(
                    [thermometer.id,
                     thermometer.name,
                     thermometer.path])

    @log_on_start(logging.INFO, 'Update used paths.')
    def update_used_paths(self) -> None:
        self.used_paths = tuple(thermometer.path
                                for thermometer in self.appliance.thermometers)

    @log_on_start(logging.INFO, 'Thermometers selection changed.')
    def thermometers_selection_on_changed(self,
                                          selection: Gtk.TreeSelection
                                          ) -> None:
        (model, new_iter) = selection.get_selected()

        if new_iter is not None:
            thermometer_id = model.get_value(new_iter, 0)

            if thermometer_id == -1:
                thermometers_count = len(self.appliance.thermometers)
                self.thermometer = Thermometer(
                    appliance=self.appliance,
                    name=f'Therm. {thermometers_count + 1}')
            else:
                self.thermometer = thermometercontroller.get_thermometer(
                    self.application.db_session,
                    thermometer_id)

            self.selected_iter = new_iter

            self.update_add_button_status(self.thermometer)
            self.update_del_button_status(self.thermometer)
            self.update_save_thermometer_button_status(self.thermometer)
            self.update_thermometer_props(self.thermometer)
        else:
            self.update_add_button_status(None)
            self.update_del_button_status(None)
            self.props_box.hide()
            self.props_control_box.hide()

    @log_on_start(logging.INFO, 'Update add button status.')
    @log_on_end(
        logging.INFO,
        'Add button status. <sensitive: {self.add_button.props.sensitive}>.')
    def update_add_button_status(self,
                                 thermometer: Optional[Thermometer]) -> None:
        if thermometer is not None:
            self.add_button.set_sensitive(thermometer.id is not None)
        else:
            self.add_button.set_sensitive(True)

    @log_on_start(logging.INFO, 'Update delete button status.')
    @log_on_end(logging.INFO,
                ('Delete button status. '
                 '<sensitive: {self.delete_button.props.sensitive}>.'))
    def update_del_button_status(self,
                                 thermometer: Optional[Thermometer]) -> None:
        if thermometer is not None:
            self.delete_button.set_sensitive(thermometer.id is not None)
        else:
            self.delete_button.set_sensitive(False)

    @log_on_start(logging.INFO, 'Update save thermometer button status.')
    @log_on_end(
        logging.INFO,
        ('Save thermometer button status.'
         '<sensitive: {self.save_thermometer_button.props.sensitive}>.'))
    def update_save_thermometer_button_status(self,
                                              thermometer: Thermometer
                                              ) -> None:
        self.save_thermometer_button.set_sensitive(self.is_valid)

    @log_on_start(logging.INFO,
                  'Update thermometer props <thermometer: {thermometer!r}>.')
    def update_thermometer_props(self,
                                 thermometer: Optional[Thermometer]) -> None:
        if thermometer is None:
            self.props_box.hide()
            self.props_control_box.hide()
        else:
            self.props_box.show_all()
            self.props_control_box.show_all()
            self.props_box.set_thermometer(thermometer)

    @log_on_end(logging.INFO, 'Can change selection <{result}>.')
    def can_change_selection(self,
                             selection: Gtk.TreeSelection,
                             model: Gtk.ListStore,
                             path: Gtk.TreePath,
                             path_currently_selected: bool,
                             *data: None) -> bool:
        if path_currently_selected is True and self.unsaved_changes is True:
            action = self.application.main_window.show_unsaved_changes()

            if action == DialogButtonType.SAVE:
                self.save_thermometer()
            elif action == DialogButtonType.DONT_SAVE:
                self.refresh_appliance()
                iter = model.get_iter(path)
                thermometer_id = model.get_value(iter, 0)
                model.set_row(iter,
                              [
                                  thermometer_id,
                                  self.thermometer.name,
                                  self.thermometer.path
                              ])
            else:
                return False

        return True

    @log_on_start(logging.INFO, 'Thermometer selection changed.')
    def thermometer_on_change(self, thermometer: Thermometer) -> None:
        self.parent.appliance_on_change(self.appliance)

        if thermometer.id is None:
            thermometer_id = -1
        else:
            thermometer_id = thermometer.id

        if thermometer_id == -1:
            name = f'<New> {thermometer.name}'
        elif self.unsaved_changes is True:
            name = f'<Modified> {thermometer.name}'
        else:
            name = thermometer.name

        self.thermometers_store.set_row(
            self.selected_iter,
            [thermometer_id,
             name,
             thermometer.path])
        self.update_save_thermometer_button_status(thermometer)

    @log_on_start(logging.INFO, 'Add button clicked.')
    def add_button_on_clicked(self, button: Gtk.Button) -> None:
        new_iter = self.thermometers_store.append(
            [-1,
             '<New thermometer>',
             None])
        self.thermometers_selection.select_iter(new_iter)
        self.parent.appliance_on_change(self.appliance)

    @log_on_start(logging.INFO, 'Delete button clicked.')
    def delete_button_on_clicked(self, button: Gtk.Button) -> None:
        (thermometer_id,
         name) = self.thermometers_store.get(self.selected_iter,
                                             0,
                                             1)
        dialog = ConfirmDialog(
            'Delete thermometer',
            f'Do you want to delete the thermometer "{name}"?',
            transient_for=self.application.main_window,
            modal=True)

        confirmation = dialog.run()
        dialog.destroy()

        if confirmation == DialogButtonType.YES:
            if thermometer_id != -1:
                thermometercontroller.delete_thermometer(
                    self.application.db_session,
                    self.thermometer)
                self.update_used_paths()
                self.thermometer = None

            iter = self.selected_iter
            self.thermometers_store.remove(iter)

    @log_on_start(
        logging.INFO,
        'Update properties visibility <selection={self.selected_iter!r}>.')
    def update_props_visibility(self) -> None:
        if self.selected_iter is None:
            self.props_box.hide()
            self.props_control_box.hide()
        else:
            self.props_box.show_all()
            self.props_control_box.show_all()

    @log_on_start(logging.INFO, 'Save thermometer button clicked')
    def save_thermometer_button_on_clicked(self, button: Gtk.Button) -> None:
        self.save_thermometer()

    @log_on_start(logging.INFO, 'Save thermometer {self.thermometer!r}')
    def save_thermometer(self) -> None:
        thermometer = self.thermometer
        thermometer.appliance = self.appliance

        thermometer = thermometercontroller.save_thermometer(
            self.application.db_session,
            thermometer)
        self.update_add_button_status(thermometer)
        self.update_del_button_status(thermometer)

        self.thermometers_store.set_row(
            self.selected_iter,
            [thermometer.id,
             thermometer.name,
             thermometer.path])

        self.parent.appliance_on_change(self.appliance)
        self.update_used_paths()

    @log_on_start(logging.INFO, 'Refresh thermometer')
    def refresh_thermometer(self) -> None:
        self.props_box.clear()
        thermometer_iter = self.selected_iter

        if thermometer_iter is not None:
            thermometer_id = self.thermometers_store.get_value(
                thermometer_iter,
                0)

            if thermometer_id == -1:
                self.thermometers_store.remove(thermometer_iter)
                self.thermometer = None
                self.selected_iter = None
            else:
                thermometercontroller.refresh_thermometer(
                    self.application.db_session,
                    self.thermometer)
                self.thermometers_store.set_row(thermometer_iter,
                                                [
                                                    self.thermometer.id,
                                                    self.thermometer.name,
                                                    self.thermometer.path
                                                ])

        self.update_props_visibility()
        self.parent.appliance_on_change(self.appliance)

    @log_on_start(logging.INFO, 'Cancel thermometer button clicked')
    def cancel_thermometer_button_on_clicked(self, button: Gtk.Button) -> None:
        self.refresh_thermometer()

    @property
    @log_on_end(logging.INFO, 'Thermometer unsaved changes: <{result}>.')
    def unsaved_changes(self) -> bool:
        if self.thermometer is not None:
            return self.application.db_session.is_modified(self.thermometer)
        else:
            return False

    @property
    @log_on_end(logging.INFO, 'Is valid thermometer <{result}>.')
    def is_valid(self) -> bool:
        if self.thermometer is not None:
            if self.thermometer.name is None or self.thermometer.name == '':
                return False

            if self.thermometer.path is None or self.thermometer.path == '':
                return False

            if thermometercontroller.is_unique_name(
                    self.application.db_session,
                    self.thermometer) is not True:
                return False

            if thermometercontroller.is_unique_path(
                    self.application.db_session,
                    self.thermometer) is not True:
                return False

        return True
