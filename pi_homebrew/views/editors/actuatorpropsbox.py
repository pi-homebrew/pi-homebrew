import logging
from typing import TYPE_CHECKING
from typing import Optional

from gi.repository import Gtk
from logdecorator import log_on_start

from pi_homebrew.models import Actuator
from pi_homebrew.models.enum import ControlType
from pi_homebrew.views.dataclasses.widgeteventhandler import WidgetEventHandler
from pi_homebrew.views.editors.appliancepropsbox import AppliancePropsBox

if TYPE_CHECKING:
    from pi_homebrew.pihomebrewapp import PiHomebrewApp


class ActuatorPropsBox(AppliancePropsBox):
    @log_on_start(logging.INFO, 'Initializing ActuatorPropsBox')
    def __init__(self,
                 application: 'PiHomebrewApp',
                 parent: Gtk.Widget) -> None:
        super().__init__(application, parent)

        self.appliance: Optional[Actuator] = None

        control_type_label = Gtk.Label('Control type:', xalign=0)
        self.pack_start(control_type_label, False, False, 0)

        self.control_type_store = Gtk.ListStore(str, str)
        self.control_type_store.append(
            [str(ControlType.ON_OFF.value),
             'On - Off'])
        self.control_type_store.append(
            [str(ControlType.PROPORTIONAL.value),
             'Proportional'])
        self.control_type_store.append(
            [str(ControlType.COMBINED.value),
             'Combined'])

        self.control_type_combo = Gtk.ComboBox.new_with_model(
            self.control_type_store)

        control_type_renderer = Gtk.CellRendererText()
        self.control_type_combo.pack_start(control_type_renderer, True)
        self.control_type_combo.add_attribute(control_type_renderer, 'text', 1)
        self.control_type_combo.set_id_column(0)
        self.pack_start(self.control_type_combo, False, False, 0)

        min_value_label = Gtk.Label('Minimum control value:', xalign=0)
        self.pack_start(min_value_label, False, False, 0)

        self.min_adjustment = Gtk.Adjustment(lower=-10000,
                                             step_increment=1,
                                             page_increment=10)
        self.min_value_spin_button = Gtk.SpinButton(
            numeric=True,
            adjustment=self.min_adjustment,
            xalign=1)

        self.pack_start(self.min_value_spin_button, False, False, 0)

        max_value_label = Gtk.Label('Maximum control value:', xalign=0)
        self.pack_start(max_value_label, False, False, 0)

        self.max_adjustment = Gtk.Adjustment(upper=10000,
                                             step_increment=1,
                                             page_increment=10)
        self.max_value_spin_button = Gtk.SpinButton(
            numeric=True,
            adjustment=self.max_adjustment,
            xalign=1)
        self.pack_start(self.max_value_spin_button, False, False, 0)

        self.handlers += (WidgetEventHandler(
            self.control_type_combo,
            'changed',
            self.control_type_combo_on_changed),
                          WidgetEventHandler(
                              self.max_adjustment,
                              'value-changed',
                              self.max_adjustment_on_value_changed),
                          WidgetEventHandler(
                              self.min_adjustment,
                              'value-changed',
                              self.min_adjustment_on_value_changed))

    @log_on_start(logging.INFO,
                  'Control type combo changed to {combo.props.active_id}.')
    def control_type_combo_on_changed(self, combo: Gtk.ComboBox) -> None:
        control_type = int(combo.get_active_id())
        self.appliance.control_type = control_type

        self.min_value_spin_button.set_sensitive(
            control_type != ControlType.ON_OFF)
        self.max_value_spin_button.set_sensitive(
            control_type != ControlType.ON_OFF)

        if control_type == ControlType.ON_OFF:
            self.min_adjustment.set_value(0)
            self.max_adjustment.set_value(0)

        self.__update_spin_button_icon(control_type,
                                       self.min_value_spin_button)
        self.__update_spin_button_icon(control_type,
                                       self.max_value_spin_button)
        self.__update_spin_button_tooltip(control_type,
                                          self.min_value_spin_button)
        self.__update_spin_button_tooltip(control_type,
                                          self.max_value_spin_button)

    def __update_spin_button_icon(self,
                                  control_type: ControlType,
                                  spin_button: Gtk.SpinButton) -> None:
        min_value = self.appliance.min_value
        max_value = self.appliance.max_value

        if control_type != ControlType.ON_OFF and min_value == max_value:
            icon_name = 'emblem-important-symbolic'
        else:
            icon_name = 'emblem-ok-symbolic'

        spin_button.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY,
                                            icon_name)

    def __update_spin_button_tooltip(self,
                                     control_type: ControlType,
                                     spin_button: Gtk.SpinButton) -> None:
        min_value = self.appliance.min_value
        max_value = self.appliance.max_value

        if control_type != ControlType.ON_OFF and min_value == max_value:
            tooltip_text = ('The minimum control value and the '
                            'maximum control value cannot be equal.')
        else:
            tooltip_text = None

        spin_button.set_tooltip_text(tooltip_text)

    def max_adjustment_on_value_changed(self,
                                        adjustment: Gtk.Adjustment) -> None:
        max_value = int(adjustment.get_value())
        control_type = int(self.control_type_combo.get_active_id())

        self.min_adjustment.set_upper(max_value - 1)
        self.appliance.max_value = max_value

        self.__update_spin_button_icon(control_type,
                                       self.min_value_spin_button)
        self.__update_spin_button_icon(control_type,
                                       self.max_value_spin_button)
        self.__update_spin_button_tooltip(control_type,
                                          self.min_value_spin_button)
        self.__update_spin_button_tooltip(control_type,
                                          self.max_value_spin_button)

    def min_adjustment_on_value_changed(self,
                                        adjustment: Gtk.Adjustment) -> None:
        min_value = int(adjustment.get_value())
        control_type = int(self.control_type_combo.get_active_id())

        self.max_adjustment.set_lower(min_value + 1)
        self.appliance.min_value = min_value

        self.__update_spin_button_icon(control_type,
                                       self.min_value_spin_button)
        self.__update_spin_button_icon(control_type,
                                       self.max_value_spin_button)
        self.__update_spin_button_tooltip(control_type,
                                          self.min_value_spin_button)
        self.__update_spin_button_tooltip(control_type,
                                          self.max_value_spin_button)

    @log_on_start(logging.INFO, 'Set actuator {actuator!r}')
    def set_appliance(self, actuator: Actuator) -> None:
        self.disconnect_handlers()

        super().set_appliance(actuator)

        self.min_adjustment.set_value(actuator.min_value)
        self.max_adjustment.set_value(actuator.max_value)
        self.control_type_combo.set_active_id(str(actuator.control_type))

        self.min_value_spin_button.set_sensitive(
            actuator.control_type != ControlType.ON_OFF)
        self.max_value_spin_button.set_sensitive(
            actuator.control_type != ControlType.ON_OFF)

        self.__update_spin_button_icon(actuator.control_type,
                                       self.min_value_spin_button)
        self.__update_spin_button_icon(actuator.control_type,
                                       self.max_value_spin_button)
        self.__update_spin_button_tooltip(actuator.control_type,
                                          self.min_value_spin_button)
        self.__update_spin_button_tooltip(actuator.control_type,
                                          self.max_value_spin_button)

        self.connect_handlers()

    @property
    def is_valid(self) -> bool:
        valid_control_type = self.appliance.control_type is not None

        if self.appliance.control_type != ControlType.ON_OFF:
            valid_value_range = (self.appliance.min_value <
                                 self.appliance.max_value)
        else:
            valid_value_range = True

        return super().is_valid and valid_control_type and valid_value_range
