import logging
from typing import TYPE_CHECKING
from typing import Optional

from gi.repository import Gtk
from logdecorator import log_on_start

from pi_homebrew.models import Kettle
from pi_homebrew.views.dataclasses.widgeteventhandler import WidgetEventHandler
from pi_homebrew.views.editors.appliancepropsbox import AppliancePropsBox

if TYPE_CHECKING:
    from pi_homebrew.pihomebrewapp import PiHomebrewApp


class KettlePropsBox(AppliancePropsBox):
    @log_on_start(logging.INFO, 'Initializing KettlePropsBox')
    def __init__(self,
                 application: 'PiHomebrewApp',
                 parent: Gtk.Widget) -> None:
        super().__init__(application, parent)

        self.appliance: Optional[Kettle] = None

        self.average_checkbox = Gtk.CheckButton.new_with_label(
            'Show average temperature')
        self.pack_start(self.average_checkbox, False, False, 0)

        self.show_all_thermometers_checkbox = Gtk.CheckButton.new_with_label(
            'Show all thermometers')
        self.pack_start(self.show_all_thermometers_checkbox, False, False, 0)

        self.handlers += (
            WidgetEventHandler(self.average_checkbox,
                               'toggled',
                               self.average_checkbox_on_toggled),
            WidgetEventHandler(self.show_all_thermometers_checkbox,
                               'toggled',
                               self.show_all_thermometers_checkbox_on_toggled),
        )

    @log_on_start(logging.INFO, 'Set kettle {kettle!r}.')
    def set_appliance(self, kettle: Kettle) -> None:
        self.disconnect_handlers()

        super().set_appliance(kettle)

        self.average_checkbox.set_active(kettle.show_average_temp)
        self.show_all_thermometers_checkbox.set_active(
            kettle.show_all_thermometers)

        self.average_checkbox.set_sensitive(
            kettle.show_all_thermometers is not None)
        self.show_all_thermometers_checkbox.set_sensitive(
            kettle.show_average_temp is not None)

        self.connect_handlers()

    @log_on_start(logging.INFO,
                  'Average checkbox toggled. {checkbox.props.active}.')
    def average_checkbox_on_toggled(self, checkbox: Gtk.CheckButton) -> None:
        if self.appliance is not None:
            is_active = checkbox.get_active()

            if not is_active:
                self.show_all_thermometers_checkbox.set_active(True)
                self.show_all_thermometers_checkbox.set_sensitive(False)
            else:
                self.show_all_thermometers_checkbox.set_sensitive(True)

            self.appliance.show_average_temp = is_active
            self.parent.appliance_on_change(self.appliance)

    @log_on_start(
        logging.INFO,
        'Show all thermometers checkbox toggled {checkbox.props.active}.')
    def show_all_thermometers_checkbox_on_toggled(self,
                                                  checkbox: Gtk.CheckButton
                                                  ) -> None:
        if self.appliance is not None:
            is_active = checkbox.get_active()

            if not is_active:
                self.average_checkbox.set_active(True)
                self.average_checkbox.set_sensitive(False)
            else:
                self.average_checkbox.set_sensitive(True)

            self.appliance.show_all_thermometers = is_active
            self.parent.appliance_on_change(self.appliance)
