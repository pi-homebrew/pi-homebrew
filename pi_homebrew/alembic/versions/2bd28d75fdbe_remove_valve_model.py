"""remove valve model

Revision ID: 2bd28d75fdbe
Revises: dcc031331203
Create Date: 2020-09-16 14:41:34.991773

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '2bd28d75fdbe'
down_revision = 'dcc031331203'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('valves')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('valves',
                    sa.Column('id',
                              sa.INTEGER(),
                              nullable=False),
                    sa.Column('control_type',
                              sa.INTEGER(),
                              nullable=False),
                    sa.ForeignKeyConstraint(
                        ['id'],
                        ['appliances.id'],
                    ),
                    sa.PrimaryKeyConstraint('id'))
    # ### end Alembic commands ###
