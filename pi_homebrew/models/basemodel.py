"""
Create declarative base for models
"""
from sqlalchemy.ext.declarative import declarative_base

BaseModel = declarative_base()
