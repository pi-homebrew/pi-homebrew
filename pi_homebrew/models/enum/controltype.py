from enum import IntFlag


class ControlType(IntFlag):
    ON_OFF = 0
    PROPORTIONAL = 1
    COMBINED = 2
