from enum import IntEnum


class ApplianceType(IntEnum):
    KETTLE = 0
    ACTUATOR = 1
    HEAT_EXCHANGER = 2
    FERMENTOR = 3
