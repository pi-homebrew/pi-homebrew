from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Integer

from pi_homebrew.models import Appliance
from pi_homebrew.models.enum import ApplianceType, ControlType


class Actuator(Appliance):
    """
    Used to activate external appliances.

    Example uses are electrovalves, pumps, etc.

    Attributes
    ----------
    id: int
        Primary Key. It's the same value as the base Appliance id.

    min_value: int
        The mimimum value that a proportional control actuator can have.
        Defaults to 0.

    man_value: int
        The maximum value that a proportional control actuator can have.
        Defaults to 100.
    """
    __tablename__ = 'actuators'
    __mapper_args__ = {'polymorphic_identity': ApplianceType.ACTUATOR}

    id = Column(Integer, ForeignKey('appliances.id'), primary_key=True)
    control_type = Column(Integer, nullable=False, default=ControlType.ON_OFF)
    min_value = Column(Integer, nullable=False, default=0)
    max_value = Column(Integer, nullable=False, default=0)

    def __repr__(self) -> str:
        super_repr = super().__repr__().replace(')>', ', ')
        return (f'{super_repr}control_type={self.control_type})>')
