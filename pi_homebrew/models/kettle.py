from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import Boolean

from pi_homebrew.models import Appliance
from pi_homebrew.models.enum import ApplianceType


class Kettle(Appliance):
    """
    Used to heat or cool liquids.

    Example uses for a kettle are a Hot Liquor Tank, a Mash & lauter tun or a
    Boiler

    Attributes
    ----------
    id: int
        Primary Key. It's the same value as the base Appliance id.

    show_all_thermometers : bool
        If True, the view will show every related thermometer.

    show_average_temperature : bool
        If this attribute is True and the kettle has more than one related
        thermometer, the view will show an average temperature entry.
    """
    __tablename__ = 'kettles'
    __mapper_args__ = {'polymorphic_identity': ApplianceType.KETTLE}

    id = Column(Integer, ForeignKey('appliances.id'), primary_key=True)
    show_all_thermometers = Column(Boolean, nullable=False, default=True)
    show_average_temp = Column(Boolean, nullable=False, default=True)

    def __repr__(self) -> str:
        super_repr = super().__repr__().replace(')>', ', ')
        return (f'{super_repr}'
                f'show_all_thermometers={self.show_all_thermometers}, '
                f'show_average_temp={self.show_average_temp}'
                f')>')
