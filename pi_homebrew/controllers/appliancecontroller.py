import logging
from typing import Any
from typing import List
from typing import Optional
from typing import Tuple
from typing import Type

from logdecorator import log_exception
from logdecorator import log_on_end
from logdecorator import log_on_start
from sqlalchemy.exc import DBAPIError
from sqlalchemy.orm import Session
from sqlalchemy.orm.exc import NoResultFound

from pi_homebrew.controllers.exc import CheckDuplicateError
from pi_homebrew.controllers.exc import CountModelInstancesError
from pi_homebrew.controllers.exc import DeleteError
from pi_homebrew.controllers.exc import LoadModelInstanceError
from pi_homebrew.controllers.exc import LoadModelListError
from pi_homebrew.controllers.exc import NotFoundError
from pi_homebrew.models import Appliance
from pi_homebrew.models.enum import ApplianceType
from pi_homebrew.models.enum import TabType


@log_on_start(logging.INFO, 'Count appliances.')
@log_on_end(logging.INFO, '{result} appliances found.')
@log_exception(logging.ERROR,
               'Error counting appliances.',
               on_exceptions=(CountModelInstancesError,
                              ),
               reraise=True)
def count_appliances(db_session: Session,
                     appliance_type: Optional[ApplianceType] = None,
                     on_tab: Optional[TabType] = None) -> int:
    criterion: Tuple[Any, ...] = tuple()

    if appliance_type is not None:
        criterion += (Appliance.appliance_type == appliance_type, )

    if on_tab is not None:
        criterion += (Appliance.on_tab == on_tab, )

    try:
        count = db_session.query(Appliance).filter(*criterion).count()
        return count
    except DBAPIError as exc:
        msg = 'Error counting appliances'
        db_session.rollback()
        raise CountModelInstancesError(msg, Appliance) from exc


@log_on_start(logging.INFO, 'Get appliances.')
@log_exception(logging.ERROR,
               'Error getting appliances.',
               on_exceptions=(LoadModelListError,
                              ),
               reraise=True)
def get_appliances(db_session: Session,
                   appliance_type: Optional[ApplianceType] = None,
                   on_tab: Optional[TabType] = None) -> List[Appliance]:
    criterion: Tuple[Any, ...] = tuple()

    if appliance_type is not None:
        criterion += (Appliance.appliance_type == appliance_type, )

    if on_tab is not None:
        criterion += (Appliance.on_tab == on_tab, )

    try:
        return db_session.query(Appliance).filter(*criterion).order_by(
            Appliance.name).all()
    except DBAPIError as exc:
        msg = f'Error loading appliances'
        db_session.rollback()
        raise LoadModelListError(msg, Appliance) from exc


@log_on_start(logging.INFO, 'Get appliance <{appliance_id:d}>.')
@log_exception(logging.ERROR,
               'Error getting appliance.',
               on_exceptions=(LoadModelInstanceError,
                              NotFoundError),
               reraise=True)
@log_on_end(logging.INFO, 'Appliance loaded {result!r}.')
def get_appliance(db_session: Session,
                  appliance_id: int,
                  appliance_class: Type[Appliance] = Appliance) -> Appliance:
    type_name = appliance_class.__name__

    try:
        return db_session.query(appliance_class).filter(
            appliance_class.id == appliance_id).one()
    except NoResultFound as exc:
        db_session.rollback()
        msg = f'{type_name} <{appliance_id}> not found.'
        raise NotFoundError(msg, Appliance, appliance_id) from exc
    except DBAPIError as exc:
        db_session.rollback()
        msg = f'Error loading {type_name.lower()} <{appliance_id}>'
        raise LoadModelInstanceError(msg, Appliance, appliance_id) from exc


@log_on_start(logging.INFO, ('Check if appliance name is unique.'))
@log_on_end(logging.INFO, 'Is unique name: {result}')
@log_exception(logging.ERROR,
               'Error checking the appliance name.',
               on_exceptions=(CheckDuplicateError,
                              ),
               reraise=True)
def is_unique_name(db_session: Session, appliance: Appliance) -> bool:
    try:
        with db_session.no_autoflush:
            found = db_session.query(Appliance).filter(
                Appliance.name == appliance.name,
                Appliance.id != appliance.id,
                Appliance.on_tab == appliance.on_tab).count()
    except DBAPIError as exc:
        db_session.rollback()
        msg = 'Error checking the appliance name'
        raise CheckDuplicateError(msg, Appliance) from exc
    else:
        return found == 0


@log_on_start(logging.INFO, 'Delete appliance {appliance!r}')
@log_exception(logging.ERROR,
               'Error deleting appliance.',
               on_exceptions=(DeleteError,
                              ),
               reraise=True)
def delete_appliance(db_session: Session, appliance: Appliance) -> None:
    try:
        db_session.delete(appliance)
        db_session.commit()
    except DBAPIError as exc:
        db_session.rollback()
        msg = f'Error deleting appliance.'
        raise DeleteError(msg) from exc


@log_on_start(logging.INFO, 'Merge appliance {appliance!r}')
@log_on_end(logging.INFO, 'Appliance merged {result!r}')
def merge_from_db(db_session: Session, appliance: Appliance) -> Appliance:
    if appliance.id is not None:
        appliance = db_session.merge(appliance)

    return appliance


@log_on_start(logging.INFO, 'Refresh appliance {appliance!r}')
@log_on_end(logging.INFO, 'Appliance refreshed {appliance!r}')
def refresh_apliance(db_session: Session, appliance: Appliance) -> Appliance:
    db_session.rollback()
    return appliance
