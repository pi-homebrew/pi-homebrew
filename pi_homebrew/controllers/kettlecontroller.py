import logging

from logdecorator import log_exception
from logdecorator import log_on_end
from logdecorator import log_on_start
from sqlalchemy.exc import DBAPIError
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

from pi_homebrew.controllers import appliancecontroller
from pi_homebrew.controllers.exc import DuplicateError
from pi_homebrew.controllers.exc import EmptyFieldError
from pi_homebrew.controllers.exc import SaveError
from pi_homebrew.models import Kettle


@log_on_start(logging.INFO, 'Save kettle {kettle!r}.')
@log_on_end(logging.INFO, 'Kettle saved {kettle!r}.')
@log_exception(logging.ERROR,
               'Error saving kettle {kettle!r}.',
               on_exceptions=(
                   EmptyFieldError,
                   SaveError,
               ),
               reraise=True)
def save_kettle(db_session: Session, kettle: Kettle) -> Kettle:
    kettle = appliancecontroller.merge_from_db(db_session, kettle)
    db_session.add(kettle)

    try:
        db_session.commit()
    except IntegrityError as exc:
        db_session.rollback()
        message = str(exc)
        if ('NOT NULL constraint failed:' in message
                or 'CHECK constraint failed: appliances' in message):
            if 'appliances.on_tab' in message:
                msg = 'A kettle must be in a Tab'
                field_name = 'on_tab'
            elif 'appliances.name' in message or kettle.name == '':
                msg = 'A kettle must have a name'
                field_name = 'name'
            else:
                msg = 'A kettle constraint failed'
                field_name = 'unknown'

            raise EmptyFieldError(msg, kettle, field_name) from exc

        if ('UNIQUE constraint failed: appliances.on_tab, '
                'appliances.name') in message:
            msg = 'A kettle name must be unique'
            raise DuplicateError(msg, kettle, 'name')

        msg = f'Error saving kettle {kettle}'
        raise SaveError(msg, kettle)
    except DBAPIError as exc:
        msg = f'Database error saving kettle {kettle}'
        raise SaveError(msg, kettle) from exc
    else:
        return kettle
