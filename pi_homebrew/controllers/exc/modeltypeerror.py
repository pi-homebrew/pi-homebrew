from typing import Type
from pi_homebrew.models.basemodel import BaseModel


class ModelTypeError(Exception):
    """
    Base exception class for errors related to model classes

    Parameters
    ----------
    message: str:
        Error description.

    model_type: :obj:`Type[BaseModel]`:
        The SqlAlchemy BaseModel subclass affected by the error.
    """
    def __init__(self, message, model_type: Type[BaseModel]):
        super().__init__(message)
        self.message = message
        self.model_type = model_type
