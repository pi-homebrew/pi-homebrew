class CheckDuplicateError(Exception):
    """
    Error launched when a duplicate record test function fails.

    Parameters
    ----------
    message: str:
        Error description.

    model_type: :obj:`Type[BaseModel] `:
        The SqlAlchemy BaseModel subclass affected by the error.
    """
    pass
