from pi_homebrew.models import BaseModel

from .modelinstanceerror import ModelInstanceError


class ModelInstanceFieldError(ModelInstanceError):
    """
    Base exception class raised for invalid field values.

    Parameters
    ----------
    message: str:
        The error message to display.

    model_instance: :obj:`BaseModel`:
        The model instance with the invalid value.

    field_name: str:
        The model field with the invalid value.
    """
    def __init__(self,
                 message: str,
                 model_instance: BaseModel,
                 field_name: str) -> None:
        super().__init__(message, model_instance)
        self.field_name = field_name
