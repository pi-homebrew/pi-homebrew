from .modelinstancefielderror import ModelInstanceFieldError


class EmptyFieldError(ModelInstanceFieldError):
    """
    Raised when a required field is empty (`None` or empty string)

    Parameters
    ----------
    message: str:
        The error message to display.

    model_instance: :obj:`BaseModel`:
        The model instance with the invalid value.

    field_name: str:
        The model field with the invalid value.
    """
    pass
