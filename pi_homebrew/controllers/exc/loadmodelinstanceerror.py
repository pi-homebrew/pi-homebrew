from typing import Any
from typing import Type

from pi_homebrew.models import BaseModel

from .modeltypeerror import ModelTypeError


class LoadModelInstanceError(ModelTypeError):
    """
    Raised when a model instance cannot be loaded from the database

    Parameters
    ----------
    message: str:
        Error description.

    model_type: :obj:`Type[BaseModel]`:
        The SqlAlchemy BaseModel subclass affected by the error.

    instance_id: :obj:`Any`:
        The instance primary key.
    """
    def __init__(self,
                 message: str,
                 model_type: Type[BaseModel],
                 instance_id: Any):
        super().__init__(message, model_type)
        self.instance_id = instance_id
