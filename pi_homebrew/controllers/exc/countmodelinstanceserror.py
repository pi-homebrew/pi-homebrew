from .modeltypeerror import ModelTypeError


class CountModelInstancesError(ModelTypeError):
    """
    Raised when a count query fails
    """
    pass
