import logging

from logdecorator import log_exception
from logdecorator import log_on_end
from logdecorator import log_on_start
from sqlalchemy.exc import DBAPIError
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

from pi_homebrew.controllers import appliancecontroller
from pi_homebrew.controllers.exc import DuplicateError
from pi_homebrew.controllers.exc import EmptyFieldError
from pi_homebrew.controllers.exc import SaveError
from pi_homebrew.models import Actuator


@log_on_start(logging.INFO, 'Get actuator <{actuator_id:d}>')
def get_actuator(db_session: Session, actuator_id: int) -> Actuator:
    return appliancecontroller.get_appliance(db_session, actuator_id, Actuator)


@log_on_start(logging.INFO, 'Save actuator {actuator!r}.')
@log_on_end(logging.INFO, 'Actuator saved {actuator!r}.')
@log_exception(logging.ERROR,
               'Error saving actuator {actuator!r}.',
               on_exceptions=(
                   EmptyFieldError,
                   SaveError,
               ),
               reraise=True)
def save_actuator(db_session: Session, actuator: Actuator) -> Actuator:
    actuator = appliancecontroller.merge_from_db(db_session, actuator)
    db_session.add(actuator)

    try:
        db_session.commit()
    except IntegrityError as exc:
        db_session.rollback()
        message = str(exc)

        if ('NOT NULL constraint failed:' in message
                or 'CHECK constraint failed: appliances' in message):
            if 'appliances.on_tab' in message:
                msg = 'An actuator must be in a Tab'
                field_name = 'on_tab'
            elif 'appliances.name' in message or actuator.name == '':
                msg = 'An actuator must have a name'
                field_name = 'name'
            else:
                msg = 'An actuator constraint failed'
                field_name = 'unknown'

            raise EmptyFieldError(msg, actuator, field_name) from exc

        if ('UNIQUE constraint failed: appliances.on_tab, '
                'appliances.name') in message:
            msg = 'An actuator name must be unique'
            raise DuplicateError(msg, actuator, 'name')

        msg = f'Error saving actuator {actuator}'
        raise SaveError(msg, actuator)
    except DBAPIError as exc:
        msg = f'Database error saving actuator {actuator}'
        raise SaveError(msg, actuator) from exc
    else:
        return actuator
