import logging
import threading
import time
from typing import Callable
from typing import Dict
from typing import Optional
from typing import Set
from typing import Tuple

from logdecorator import log_exception
from logdecorator import log_on_end
from logdecorator import log_on_start
from owclient import OwClient
from owclient.devices import Thermometer
from owclient.exc import CouldNotReadError


class ThermometerScanner(threading.Thread):
    @log_on_start(logging.INFO, 'Initialize Thermometer scanner instance.')
    def __init__(self, scan_interval: float = 5, **kwargs) -> None:

        threading.Thread.__init__(self)

        self.stop = False
        self.callback: Optional[Callable[
            [Dict[str,
                  Optional[float]],
             float,
             float],
            None]] = None
        self.scan_interval = scan_interval
        self.thermometers: Dict[str, Thermometer] = dict()
        self.daemon = True

    @log_on_start(logging.INFO, 'Delay next read {delay:.2f}')
    def __delay_next_reading(self, delay: float) -> None:
        time.sleep(delay)

    @log_on_start(logging.INFO, 'Reading {self.thermometers!r} thermometers.')
    @log_on_end(logging.INFO, 'Reading complete, duration: {result[1]:.2f}')
    def __read_thermometers(
        self,
        thermometers: Dict[str,
                           Thermometer]
    ) -> Tuple[Dict[str,
                    Optional[float]],
               float,
               float]:
        start = time.time()
        temperatures: Dict[str, Optional[float]] = dict()

        for (path, thermometer) in thermometers.items():
            if self.stop:
                break

            temperatures[path] = self.__read_thermometer(thermometer)

        end = time.time()
        duration = end - start
        delay = max(self.scan_interval - duration, 0)

        return temperatures, duration, delay

    @log_on_start(logging.INFO, 'Reading thermometer {thermometer.path!s}')
    @log_exception(logging.ERROR,
                   'Error reading thermometer {thermometer.path!s}',
                   on_exceptions=CouldNotReadError)
    def __read_thermometer(self, thermometer: Thermometer) -> Optional[float]:
        return thermometer.temperature

    @log_on_start(logging.INFO, 'Start thermometer readings loop.')
    @log_on_end(logging.INFO, 'End thermometer readings loop.')
    def run(self) -> None:
        delay = 0.0

        while not self.stop:
            self.__delay_next_reading(delay)
            (temperatures,
             duration,
             delay) = self.__read_thermometers(self.thermometers)

            if not self.stop and self.callback is not None:
                self.callback(temperatures, duration, delay)
