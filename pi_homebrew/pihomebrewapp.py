import logging
import os
from typing import Callable
from typing import Dict
from typing import List
from typing import Optional
from typing import Set
from typing import Tuple

from gi.repository import Gio
from gi.repository import GLib
from gi.repository import Gtk
from logdecorator import log_on_end
from logdecorator import log_on_start
from owclient.devices import Thermometer as ThermometerDevice
from sqlalchemy.engine import Engine as DBEngine
from sqlalchemy.exc import OperationalError

from pi_homebrew.controllers import appliancecontroller
from pi_homebrew.controllers import dbcontroller
from pi_homebrew.controllers import thermometercontroller
from pi_homebrew.controllers.enum import BrewSteps
from pi_homebrew.controllers.thermometerscanner import ThermometerScanner
from pi_homebrew.models import Appliance
from pi_homebrew.views import ConfirmDialog
from pi_homebrew.views import ErrorDialog
from pi_homebrew.views import MainWindow
from pi_homebrew.views.enum import DialogButtonType
from concurrent import futures


class PiHomebrewApp(Gtk.Application):
    """
    PiHomebrew application class
    """
    @log_on_start(logging.INFO, 'Init PiHomebrewApp class')
    def __init__(self) -> None:
        Gtk.Application.__init__(self,
                                 application_id='org.example.pi_homebrew')

        GLib.set_application_name('Pi homebrew')
        GLib.set_prgname('org.example.pi_homebrew')

        self.db_filename = 'pi-homebrew.sqlite'
        self.db_url = f'sqlite:///{self.db_filename}'
        self.main_window: Optional[MainWindow] = None
        self.engine: Optional[DBEngine] = None
        self.appliances: List[Appliance] = []

        self.current_step = BrewSteps.IDLE
        self.scanner = ThermometerScanner()
        self.scanner.callback = self.scan_on_complete
        self.on_read_done_callbacks: Tuple(
            Callable[[Dict[str,
                           Optional[float]]],
                     None],
            ...) = tuple()

    @log_on_start(logging.INFO, 'Application startup')
    def do_startup(self) -> None:
        Gtk.Application.do_startup(self)

    @log_on_start(logging.INFO, 'Application shutdown')
    def do_shutdown(self):
        if self.engine is not None:
            dbcontroller.dispose_engine(self.engine)

        Gtk.Application.do_shutdown(self)

    @log_on_start(logging.INFO, 'Application activate')
    def do_activate(self):
        # We only allow a single window and raise any existing ones
        if not self.main_window:
            (self.db_engine,
             self.db_session) = dbcontroller.connect_db(self.db_url)

            self.main_window = MainWindow(self, title='Main Window')

            try:
                script_path = os.getcwd()
                dbcontroller.check_db(self.db_engine,
                                      self.db_filename,
                                      script_path)
            except OperationalError:
                error_dialog = ErrorDialog(
                    'Database corrupt.',
                    ('Database corrupt, please delete the file '
                     'pi-homebrew.sqlite in the application folder and '
                     'restart.'),
                    transient_for=self.main_window)
                error_dialog.run()
                self.quit()
                return
            else:
                self.main_window.start()

        self.main_window.show()

    @log_on_start(logging.INFO, 'Show about dialog')
    def on_about(self, action: Gio.SimpleAction, param=None):
        about_dialog = Gtk.AboutDialog(transient_for=self.main_window,
                                       modal=True,
                                       program_name='Pi homebrew',
                                       authors=['Ferran Comabella'])
        about_dialog.show_all()

    @log_on_start(logging.INFO, 'Refresh appliances')
    def refresh_appliances(self) -> None:
        self.appliances = appliancecontroller.get_appliances(self.db_session)

    @log_on_start(logging.INFO, 'Go to next step.')
    def next_step(self) -> None:
        if self.current_step == BrewSteps.IDLE:
            do_start = self.confirm_start_brewing()

            if do_start == DialogButtonType.YES:
                self.main_window.config_button.hide()
                self.start_brewing_session()
        else:
            pass

    @log_on_start(logging.INFO, 'Show start brewing confirmation dialog')
    @log_on_end(logging.INFO, 'Dialog result: {result}')
    def confirm_start_brewing(self) -> DialogButtonType:
        confirm_dialog = ConfirmDialog(
            'New brewing session.',
            'Do you want to start a brewing session?',
            transient_for=self.main_window,
            modal=True)
        do_start = confirm_dialog.run()
        confirm_dialog.destroy()
        return do_start

    @log_on_start(logging.INFO, 'Start brewing session')
    def start_brewing_session(self) -> None:
        next_step = self.current_step + 1
        thermometer_devices_future = self.load_step_thermometers(next_step)
        thermometer_devices_future.add_done_callback(
            self.set_scanner_thermometers)
        self.scanner.start()
        self.current_step = next_step
        self.update_next_step_button_status()

    @log_on_start(logging.INFO, 'Load step <{step:d}> thermometers.')
    def load_step_thermometers(self, step: BrewSteps) -> futures.Future:
        step_thermometers = thermometercontroller.get_step_thermometers(
            self.db_session,
            step)

        return thermometercontroller.load_thermometer_devices(
            step_thermometers)

    @log_on_end(logging.INFO,
                'Set scanner thermometers <{self.scanner.thermometers!r}>.')
    def set_scanner_thermometers(self, devices_future: futures.Future) -> None:
        self.scanner.thermometers = devices_future.result()

    def update_next_step_button_status(self) -> None:
        pass

    @log_on_start(logging.INFO,
                  ('Scan complete <temperatures={temperatures!r}, '
                   'duration={duration:.3f}, '
                   'delay={delay:.3f}>.'))
    def scan_on_complete(self,
                         temperatures: Dict[str,
                                            Optional[float]],
                         duration: float,
                         delay: float) -> None:
        for callback in self.on_read_done_callbacks:
            callback(temperatures)
