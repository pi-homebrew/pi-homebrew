import logging
from typing import Callable
from typing import Iterable
from typing import List
from typing import NoReturn
from typing import Tuple

import owclient  # type: ignore
import pytest
from owclient.exc import CouldNotConnectError  # type:ignore
from owclient.exc import CouldNotLoadDeviceError  # type:ignore
from owclient.exc import CouldNotLoadDevicesError  # type:ignore
from owclient.exc import CouldNotReadError  # type:ignore
from sqlalchemy import event
from sqlalchemy.engine import Connection
from sqlalchemy.engine import Engine
from sqlalchemy.engine import create_engine
from sqlalchemy.exc import DBAPIError
from sqlalchemy.orm.session import Session

from pi_homebrew.models import BaseModel
from pi_homebrew.models import Kettle
from pi_homebrew.models import Thermometer
from pi_homebrew.models.enum import TabType


@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()


@pytest.fixture(scope='module')
def db_engine() -> Iterable[Engine]:
    logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)
    engine = create_engine('sqlite:///:memory:')
    yield engine
    engine.dispose()


@pytest.fixture(scope='module')
def db_connection(db_engine) -> Iterable[Connection]:
    connection = db_engine.connect()
    transaction = connection.begin()
    BaseModel.metadata.create_all(connection)
    yield connection
    transaction.rollback()
    connection.close()


@pytest.fixture
def db_session(db_connection: Connection) -> Iterable[Session]:
    transaction = db_connection.begin_nested()
    session = Session(bind=db_connection)
    yield session
    session.close()
    transaction.rollback()


@pytest.fixture
def test_kettle(db_session: Session) -> Kettle:
    kettle = Kettle(name='Test Kettle', on_tab=TabType.BREW_HOUSE)
    db_session.add(kettle)
    db_session.commit()
    return kettle


@pytest.fixture
def test_thermometer(db_session: Session, test_kettle: Kettle) -> Thermometer:
    thermometer = Thermometer(name='Test thermometer',
                              path='test_path',
                              appliance=test_kettle)
    db_session.add(thermometer)
    db_session.commit()
    return thermometer


@pytest.fixture
def multiple_kettles(db_session: Session) -> Callable[[int], List[Kettle]]:
    def _generate_multiple_kettles(
            count: int = 1,
            on_tab: TabType = TabType.BREW_HOUSE) -> List[Kettle]:
        kettles = [
            Kettle(name=f'Test kettle {index}',
                   on_tab=on_tab) for index in range(count)
        ]
        db_session.add_all(kettles)
        db_session.commit()
        return kettles

    return _generate_multiple_kettles


@pytest.fixture
def multiple_thermometers(
        db_session: Session,
        test_kettle: Kettle) -> Callable[[int],
                                         List[Thermometer]]:
    def _generate_multiple_thermometers(count: int = 1) -> List[Thermometer]:
        db_session.add_all([
            Thermometer(name=f'Test thermometer {index}',
                        path=f'test_path_{index}',
                        appliance=test_kettle) for index in range(count)
        ])
        db_session.commit()
        return db_session.query(Thermometer).all()

    return _generate_multiple_thermometers


@pytest.fixture
def raise_dbapierror() -> Callable[..., NoReturn]:
    def do_raise(*args, **kwargs) -> NoReturn:
        raise DBAPIError(None, None, None, False)

    return do_raise


@pytest.fixture
def patch_owclient(monkeypatch):
    class MockDevice:
        def __init__(self, path: str) -> None:
            self.path = path
            self.__temperature = 0

        @property
        def temperature(self) -> float:
            if onewire.raise_on_read:
                raise CouldNotReadError()

            if onewire.temperature_delay > 0:
                import time
                time.sleep(onewire.temperature_delay)
            self.__temperature += 1
            return self.__temperature

    class MockOwClient:
        def load_device(self, path: str) -> MockDevice:
            if onewire.raise_connect_error:
                raise CouldNotConnectError()

            if onewire.raise_on_load:
                raise CouldNotLoadDeviceError()

            if path not in onewire.devices:
                raise CouldNotLoadDeviceError()
            else:
                return MockDevice(path)

        @property
        def devices(self) -> Tuple[MockDevice, ...]:
            if onewire.raise_on_load:
                raise CouldNotLoadDevicesError()

            return tuple(MockDevice(path) for path in onewire.devices)

    class OneWire:
        def __init__(self):
            self.devices = set()
            self.raise_on_load = False
            self.raise_connect_error = False
            self.raise_on_read = False
            self.temperature_delay = 0.0

    onewire = OneWire()

    monkeypatch.setattr(owclient, 'OwClient', MockOwClient)
    monkeypatch.setattr(owclient.devices, 'Thermometer', MockDevice)

    return onewire, MockOwClient, MockDevice
