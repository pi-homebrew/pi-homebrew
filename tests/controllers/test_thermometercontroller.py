from concurrent import futures
from typing import Callable
from typing import List
from typing import NoReturn

import pytest
from owclient.exc import CouldNotLoadDeviceError  # type:ignore
from sqlalchemy.orm import Session

from pi_homebrew.controllers import thermometercontroller
from pi_homebrew.controllers.exc import CheckDuplicateError
from pi_homebrew.controllers.exc import DuplicateError
from pi_homebrew.controllers.exc import EmptyFieldError
from pi_homebrew.controllers.exc import NotFoundError
from pi_homebrew.controllers.exc import SaveError
from pi_homebrew.controllers.exc import LoadModelInstanceError
from pi_homebrew.controllers.exc import DeleteError
from pi_homebrew.models import Kettle
from pi_homebrew.models import Thermometer


def test_is_used_path_fails_dbapierror(db_session: Session,
                                       test_thermometer: Thermometer,
                                       raise_dbapierror: Callable[...,
                                                                  NoReturn],
                                       monkeypatch) -> None:
    appliance = test_thermometer.appliance
    monkeypatch.setattr(db_session, 'query', raise_dbapierror)

    with pytest.raises(CheckDuplicateError):
        thermometercontroller.is_used_path(db_session,
                                           test_thermometer,
                                           appliance)


def test_is_used_path_existing_path(test_thermometer: Thermometer,
                                    db_session: Session) -> None:
    appliance = test_thermometer.appliance

    assert thermometercontroller.is_used_path(db_session,
                                              test_thermometer,
                                              appliance) is True


def test_is_used_path_nonexisting_path(test_thermometer: Thermometer,
                                       db_session: Session) -> None:
    appliance = test_thermometer.appliance
    nt = Thermometer(name='second',
                     appliance_id=test_thermometer.appliance_id,
                     path='new_path')

    assert thermometercontroller.is_used_path(db_session,
                                              nt,
                                              appliance) is False


def test_is_used_path_ignores_exclude(db_session: Session,
                                      test_thermometer: Thermometer) -> None:
    assert thermometercontroller.is_used_path(db_session,
                                              test_thermometer,
                                              test_thermometer.appliance,
                                              test_thermometer) is False


def test_is_unique_name_dbapierror(db_session: Session,
                                   raise_dbapierror: Callable[...,
                                                              NoReturn],
                                   test_thermometer: Thermometer,
                                   monkeypatch) -> None:
    monkeypatch.setattr(db_session, 'query', raise_dbapierror)

    with pytest.raises(CheckDuplicateError):
        thermometercontroller.is_unique_name(db_session, test_thermometer)


def test_is_unique_name_finds_duplicate(db_session: Session,
                                        test_thermometer: Thermometer) -> None:
    dup = Thermometer(appliance_id=test_thermometer.appliance_id,
                      name=test_thermometer.name)
    assert thermometercontroller.is_unique_name(db_session, dup) is False


def test_is_unique_name_ignores_the_thermometer(
        db_session: Session,
        multiple_thermometers: Callable[[int],
                                        List[Thermometer]]) -> None:
    thermometers = multiple_thermometers(2)
    assert thermometercontroller.is_unique_name(db_session, thermometers[0])


def test_is_unique_path_dbapierror(db_session: Session,
                                   raise_dbapierror: Callable[...,
                                                              NoReturn],
                                   test_thermometer: Thermometer,
                                   monkeypatch) -> None:
    monkeypatch.setattr(db_session, 'query', raise_dbapierror)

    with pytest.raises(CheckDuplicateError):
        thermometercontroller.is_unique_path(db_session, test_thermometer)


def test_is_unique_path_finds_duplicate(db_session: Session,
                                        test_thermometer: Thermometer) -> None:
    dup = Thermometer(appliance_id=test_thermometer.appliance_id,
                      name='Test thermometer 2',
                      path=test_thermometer.path)
    assert thermometercontroller.is_unique_path(db_session, dup) is False


def test_is_unique_path_ignores_the_thermometer(
        db_session: Session,
        multiple_thermometers: Callable[[int],
                                        List[Thermometer]]) -> None:
    thermometers = multiple_thermometers(2)
    assert thermometercontroller.is_unique_path(db_session, thermometers[0])


def test_save_thermometer_fails_appliance_id_is_none(
        db_session: Session) -> None:
    nt = Thermometer(name='Test thermometer', path='test_path')

    with pytest.raises(EmptyFieldError):
        thermometercontroller.save_thermometer(db_session, nt)


def test_save_thermometer_fails_name_is_none(db_session: Session,
                                             test_kettle: Kettle) -> None:
    nt = Thermometer(appliance=test_kettle, path='test_path')
    with pytest.raises(EmptyFieldError):
        thermometercontroller.save_thermometer(db_session, nt)


def test_save_thermometer_fails_name_is_empty(db_session: Session,
                                              test_kettle: Kettle) -> None:
    nt = Thermometer(appliance=test_kettle, name='', path='test_path')

    with pytest.raises(EmptyFieldError):
        thermometercontroller.save_thermometer(db_session, nt)


def test_save_thermometer_fails_path_is_none(db_session: Session,
                                             test_kettle: Kettle) -> None:
    nt = Thermometer(appliance=test_kettle, name='Test thermometer')

    with pytest.raises(EmptyFieldError):
        thermometercontroller.save_thermometer(db_session, nt)


def test_save_thermometer_fails_path_is_empty(db_session: Session,
                                              test_kettle: Kettle) -> None:
    nt = Thermometer(appliance=test_kettle, name='Test thermometer', path='')

    with pytest.raises(EmptyFieldError):
        thermometercontroller.save_thermometer(db_session, nt)


def test_save_thermometer_fails_duplicate_name(db_session: Session,
                                               test_thermometer: Thermometer
                                               ) -> None:
    dup = Thermometer(name=test_thermometer.name,
                      path='test_path_2',
                      appliance=test_thermometer.appliance)

    with pytest.raises(DuplicateError):
        thermometercontroller.save_thermometer(db_session, dup)


def test_save_thermometer_fails_duplicate_path(db_session: Session,
                                               test_thermometer: Thermometer
                                               ) -> None:
    dup = Thermometer(name='Duplicate path',
                      appliance=test_thermometer.appliance,
                      path=test_thermometer.path)

    with pytest.raises(DuplicateError):
        thermometercontroller.save_thermometer(db_session, dup)


def test_save_thermometer_dbapierror(db_session: Session,
                                     raise_dbapierror: Callable[...,
                                                                NoReturn],
                                     test_kettle: Kettle,
                                     monkeypatch) -> None:
    tt = Thermometer(appliance=test_kettle,
                     name='test thermometer',
                     path='test_path')
    monkeypatch.setattr(db_session, 'commit', raise_dbapierror)

    with pytest.raises(SaveError):
        thermometercontroller.save_thermometer(db_session, tt)


def test_save_thermometer_fails_non_existing_appliance(
        db_session: Session) -> None:
    tt = Thermometer(appliance_id=-1,
                     name='Test thermometer',
                     path='test_path')

    with pytest.raises(SaveError):
        thermometercontroller.save_thermometer(db_session, tt)


def test_save_thermometer_adds_thermometer(db_session: Session,
                                           test_kettle: Kettle) -> None:
    nt = Thermometer(name='Test thermometer',
                     path='test_path',
                     appliance=test_kettle)
    saved = thermometercontroller.save_thermometer(db_session, nt)

    assert saved is nt
    assert saved.id is not None, 'A saved thermometer must have an id'


def test_save_thermometer_merges_thermometer(db_session: Session,
                                             test_thermometer: Thermometer
                                             ) -> None:
    new = Thermometer(id=test_thermometer.id, name='Merge thermometer')
    saved = thermometercontroller.save_thermometer(db_session, new)

    assert saved.id == test_thermometer.id
    assert saved is test_thermometer


def test_load_thermometer_not_found(db_session: Session,
                                    test_thermometer: Thermometer) -> None:
    with pytest.raises(NotFoundError):
        thermometercontroller.get_thermometer(db_session, -1)


def test_load_thermometer_dbapierror(db_session: Session,
                                     raise_dbapierror: Callable[...,
                                                                NoReturn],
                                     test_thermometer: Thermometer,
                                     monkeypatch) -> None:
    thermometer_id = test_thermometer.id
    monkeypatch.setattr(db_session, 'query', raise_dbapierror)

    with pytest.raises(LoadModelInstanceError):
        thermometercontroller.get_thermometer(db_session, thermometer_id)


def test_load_thermometer(test_thermometer: Thermometer,
                          db_session: Session) -> None:
    loaded = thermometercontroller.get_thermometer(db_session,
                                                   test_thermometer.id)

    assert isinstance(loaded, Thermometer)
    assert loaded is test_thermometer


def test_delete_thermometer(test_thermometer: Thermometer,
                            db_session: Session) -> None:
    thermometercontroller.delete_thermometer(db_session, test_thermometer)

    with pytest.raises(NotFoundError):
        thermometercontroller.get_thermometer(db_session, test_thermometer.id)


def test_delete_thermometer_dbapierror(test_thermometer: Thermometer,
                                       db_session: Session,
                                       raise_dbapierror: Callable[...,
                                                                  NoReturn],
                                       monkeypatch) -> None:
    monkeypatch.setattr(db_session, 'delete', raise_dbapierror)

    with pytest.raises(DeleteError):
        thermometercontroller.delete_thermometer(db_session, test_thermometer)


def test_load_thermometer_devices_returns_future(patch_owclient) -> None:
    (ow_config, MockOwClient, MockDevice) = patch_owclient
    ow_config.devices = {f'test-device-{device}' for device in range(3)}

    devices_future = thermometercontroller.load_thermometer_devices()

    assert isinstance(devices_future, futures.Future)


def test_load_thermometer_devices_lists_all_devices(patch_owclient) -> None:
    (ow_config, MockOwClient, MockDevice) = patch_owclient
    ow_config.devices = {f'test-device-{device}' for device in range(3)}

    devices_future = thermometercontroller.load_thermometer_devices()
    devices = devices_future.result()

    assert len(devices) == len(ow_config.devices)

    for (path, device) in devices.items():
        assert path in ow_config.devices
        assert isinstance(device, MockDevice)


def test_load_thermometers_lists_specified_paths(patch_owclient) -> None:
    (ow_config, MockOwClient, MockDevice) = patch_owclient
    ow_config.devices = {f'test-device-{device}' for device in range(3)}
    specified_paths = {'test-device-0', 'test-device-1'}
    devices_future = thermometercontroller.load_thermometer_devices(
        specified_paths)

    devices = devices_future.result()

    assert len(devices) == len(specified_paths)

    for (path, device) in devices.items():
        assert path in ow_config.devices
        assert isinstance(device, MockDevice)


def test_load_thermometers_unexisting_device(patch_owclient) -> None:
    (ow_config, MockOwClient, MockDevice) = patch_owclient
    ow_config.devices = {f'test-device-{device}' for device in range(3)}
    devices_future = thermometercontroller.load_thermometer_devices(
        {'bad-device'})

    with pytest.raises(CouldNotLoadDeviceError):
        devices_future.result()
