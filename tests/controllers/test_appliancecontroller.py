from typing import Callable
from typing import List
from typing import NoReturn
from typing import Optional

import pytest
from sqlalchemy.orm import Session

from pi_homebrew.controllers import appliancecontroller
from pi_homebrew.controllers.exc import CheckDuplicateError
from pi_homebrew.controllers.exc import CountModelInstancesError
from pi_homebrew.controllers.exc import DeleteError
from pi_homebrew.controllers.exc import LoadModelInstanceError
from pi_homebrew.controllers.exc import LoadModelListError
from pi_homebrew.controllers.exc import NotFoundError
from pi_homebrew.models import Appliance
from pi_homebrew.models import Kettle
from pi_homebrew.models.enum import TabType


def test_load_appliance_no_appliances(db_session: Session) -> None:
    with pytest.raises(NotFoundError) as exc:
        appliancecontroller.get_appliance(db_session, 0)

        assert exc.model_type == Appliance
        assert exc.instance_id == 0


def test_load_appliance_not_existing(db_session: Session) -> None:
    with pytest.raises(NotFoundError) as exc:
        appliancecontroller.get_appliance(db_session, 0)

        assert exc.model_type == Appliance
        assert exc.instance_id == 0


def test_load_appliance_database_error(db_session: Session,
                                       test_kettle: Kettle,
                                       raise_dbapierror: Callable[...,
                                                                  NoReturn],
                                       monkeypatch) -> None:
    test_id = test_kettle.id
    monkeypatch.setattr(db_session, 'query', raise_dbapierror)

    with pytest.raises(LoadModelInstanceError) as exc:
        appliancecontroller.get_appliance(db_session, test_id)

        assert exc.model_type == Appliance
        assert exc.instance_id == test_id


def test_load_appliance(test_kettle: Kettle, db_session: Session) -> None:
    test_id = test_kettle.id

    appliance = appliancecontroller.get_appliance(db_session, test_id)

    assert isinstance(appliance, Appliance)
    assert appliance is test_kettle


def test_is_unique_name_database_error(db_session: Session,
                                       test_kettle: Kettle,
                                       raise_dbapierror: Callable[...,
                                                                  NoReturn],
                                       monkeypatch) -> None:
    monkeypatch.setattr(db_session, 'query', raise_dbapierror)

    with pytest.raises(CheckDuplicateError) as exc:
        appliancecontroller.is_unique_name(db_session, test_kettle)

        assert exc.message == 'Could not check if appliance name is unique'
        assert exc.model_type == Appliance


def test_is_unique_name_finds_duplicate(test_kettle: Kettle,
                                        db_session: Session) -> None:
    dup = Kettle(name=test_kettle.name, on_tab=test_kettle.on_tab)

    assert appliancecontroller.is_unique_name(db_session, dup) is False


def test_is_unique_name_ignores_the_appliance(multiple_kettles: Callable[
    [int],
        List[Kettle]],
                                              db_session: Session) -> None:
    kettles = multiple_kettles(2)
    assert appliancecontroller.is_unique_name(db_session, kettles[0]) is True


def test_delete_appliance(test_kettle: Kettle, db_session: Session) -> None:
    appliance_id = test_kettle.id
    appliancecontroller.delete_appliance(db_session, test_kettle)

    with pytest.raises(NotFoundError):
        appliancecontroller.get_appliance(db_session, appliance_id)


def test_delete_appliance_raises_dbapierror(db_session: Session,
                                            raise_dbapierror: Callable[
                                                ...,
                                                NoReturn],
                                            test_kettle: Kettle,
                                            monkeypatch) -> None:
    monkeypatch.setattr(db_session, 'delete', raise_dbapierror)

    with pytest.raises(DeleteError):
        appliancecontroller.delete_appliance(db_session, test_kettle)


@pytest.mark.parametrize('appliances_count', [0, 5])
def test_count_appliances(
        appliances_count: int,
        db_session: Session,
        multiple_kettles: Callable[[int],
                                   List[Kettle]]) -> None:
    kettles = multiple_kettles(appliances_count)

    kettle_count = appliancecontroller.count_appliances(db_session)

    assert kettle_count == len(kettles)


@pytest.mark.parametrize('on_brewhouse_tab,on_fermentation_tab',
                         [(0,
                           0),
                          (0,
                           1),
                          (1,
                           0),
                          (1,
                           1),
                          (8,
                           3)])
def test_count_appliances_by_tab(
    on_brewhouse_tab: int,
    on_fermentation_tab: int,
    db_session: Session,
    multiple_kettles: Callable[[int,
                                Optional[TabType]],
                               List[Kettle]]
) -> None:
    brewhouse_kettles = multiple_kettles(on_brewhouse_tab)
    fermentation_kettles = multiple_kettles(on_fermentation_tab,
                                            TabType.FERMENTATION)

    total_count = appliancecontroller.count_appliances(db_session)
    brewhouse_count = appliancecontroller.count_appliances(
        db_session,
        on_tab=TabType.BREW_HOUSE)
    fermentation_count = appliancecontroller.count_appliances(
        db_session,
        on_tab=TabType.FERMENTATION)

    assert brewhouse_count == len(brewhouse_kettles)
    assert fermentation_count == len(fermentation_kettles)
    assert total_count == len(brewhouse_kettles) + len(fermentation_kettles)


def test_count_appliances_raises_dbapierror(db_session: Session,
                                            raise_dbapierror: Callable[
                                                ...,
                                                NoReturn],
                                            monkeypatch) -> None:
    monkeypatch.setattr(db_session, 'query', raise_dbapierror)

    with pytest.raises(CountModelInstancesError):
        appliancecontroller.count_appliances(db_session)


@pytest.mark.parametrize('appliances_count', [0, 5])
def test_get_appliances(
        appliances_count: int,
        db_session: Session,
        multiple_kettles: Callable[[int],
                                   List[Kettle]]) -> None:
    kettles = multiple_kettles(appliances_count)

    controller_kettles = appliancecontroller.get_appliances(db_session)

    assert controller_kettles == kettles


def test_get_appliances_raises_dbapierror(db_session: Session,
                                          raise_dbapierror: Callable[...,
                                                                     NoReturn],
                                          monkeypatch) -> None:
    monkeypatch.setattr(db_session, 'query', raise_dbapierror)

    with pytest.raises(LoadModelListError):
        appliancecontroller.get_appliances(db_session)
