from typing import Callable
from typing import NoReturn

import pytest
from sqlalchemy.orm import Session

from pi_homebrew.controllers import kettlecontroller
from pi_homebrew.controllers.exc import DuplicateError
from pi_homebrew.controllers.exc import EmptyFieldError
from pi_homebrew.controllers.exc import SaveError
from pi_homebrew.models import Kettle
from pi_homebrew.models.enum import TabType


def test_save_kettle_fails_tab_id_is_none(db_session: Session) -> None:
    kettle = Kettle(name='test kettle')

    with pytest.raises(EmptyFieldError):
        kettlecontroller.save_kettle(db_session, kettle)


def test_save_kettle_fails_name_is_none(db_session: Session) -> None:
    kettle = Kettle(on_tab=TabType.BREW_HOUSE)

    with pytest.raises(EmptyFieldError):
        kettlecontroller.save_kettle(db_session, kettle)


def test_save_kettle_fails_name_is_empty(db_session: Session) -> None:
    kettle = Kettle(on_tab=TabType.BREW_HOUSE, name='')

    with pytest.raises(EmptyFieldError):
        kettlecontroller.save_kettle(db_session, kettle)


def test_save_kettle_fails_duplicate_name(test_kettle: Kettle,
                                          db_session: Session) -> None:
    dup = Kettle(name=test_kettle.name, on_tab=TabType.BREW_HOUSE)

    with pytest.raises(DuplicateError):
        kettlecontroller.save_kettle(db_session, dup)


def test_save_kettle_fails_dbapierror(
        db_session: Session,
        monkeypatch,
        raise_dbapierror: Callable[...,
                                   NoReturn]) -> None:
    kettle = Kettle(on_tab=TabType.BREW_HOUSE, name='Test kettle')

    monkeypatch.setattr(db_session, 'commit', raise_dbapierror)

    with pytest.raises(SaveError):
        kettlecontroller.save_kettle(db_session, kettle)


def test_save_kettle_adds_kettle(db_session: Session) -> None:
    nk = Kettle(on_tab=TabType.BREW_HOUSE, name='Test kettle')
    saved = kettlecontroller.save_kettle(db_session, nk)

    assert saved is nk
    assert saved.id is not None, 'A saved kettle must have an id'


def test_save_kettle_merges_kettle(db_session: Session,
                                   test_kettle: Kettle) -> None:
    new = Kettle(id=test_kettle.id, name='Merge kettle')
    saved = kettlecontroller.save_kettle(db_session, new)

    assert saved.id == test_kettle.id
    assert saved is test_kettle
    assert test_kettle.name == 'Merge kettle'
