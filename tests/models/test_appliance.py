import pytest
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.session import Session

from pi_homebrew.models import Appliance


def test_cannot_insert_appliance(db_session: Session) -> None:
    with pytest.raises(IntegrityError) as excinfo:
        new_appliance = Appliance(name='Test appliance',
                                  description='Test appliance description')
        db_session.add(new_appliance)
        db_session.commit()

    assert 'NOT NULL constraint failed: appliances.appliance_type' in \
        str(excinfo.value)
