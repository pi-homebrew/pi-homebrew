import pytest
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.session import Session

from pi_homebrew.models import Kettle
from pi_homebrew.models import Thermometer
from pi_homebrew.models.enum import TabType


def test_correct(db_session: Session, test_kettle: Kettle) -> None:
    new_thermometer = Thermometer(name='Test thermometer',
                                  path='test_path',
                                  appliance=test_kettle)
    db_session.add(new_thermometer)
    db_session.commit()

    assert new_thermometer.id is not None
    assert new_thermometer.name == 'Test thermometer'
    assert new_thermometer.path == 'test_path'
    assert new_thermometer.description == ''
    assert new_thermometer.appliance is test_kettle


def test_must_have_appliance_id(db_session: Session) -> None:
    with pytest.raises(IntegrityError) as excinfo:
        new_thermometer = Thermometer(name='Test thermometer',
                                      path='test_path')
        db_session.add(new_thermometer)
        db_session.commit()

    assert 'NOT NULL constraint failed: thermometers.appliance_id' in str(
        excinfo.value)


def test_thermometer_repr(db_session: Session, test_kettle: Kettle) -> None:
    new_thermometer = Thermometer(name='Test thermometer',
                                  path='test_path',
                                  appliance_id=test_kettle.id,
                                  description='Test thermometer description')

    assert repr(new_thermometer) == (
        f'<Thermometer ('
        f'appliance_id={test_kettle.id}, '
        f'id=None, '
        f'name=Test thermometer, '
        f'description=Test thermometer description, '
        f'path=test_path'
        f')>')


def test_name_not_null(db_session: Session, test_kettle: Kettle) -> None:
    new_thermometer = Thermometer(path='test_path', appliance=test_kettle)
    db_session.add(new_thermometer)

    with pytest.raises(IntegrityError) as excinfo:
        db_session.commit()

    assert 'NOT NULL constraint failed: thermometers.name' in str(
        excinfo.value)


def test_name_not_empty_string(db_session: Session,
                               test_kettle: Kettle) -> None:
    new_thermometer = Thermometer(name='',
                                  path='test_path',
                                  appliance=test_kettle)
    db_session.add(new_thermometer)

    with pytest.raises(IntegrityError) as excinfo:
        db_session.commit()

    assert 'CHECK constraint failed: thermometers' in str(excinfo.value)


def test_path_not_null(db_session: Session, test_kettle: Kettle) -> None:
    new_thermometer = Thermometer(name='Test thermometer',
                                  appliance=test_kettle)
    db_session.add(new_thermometer)

    with pytest.raises(IntegrityError) as excinfo:
        db_session.commit()

    assert 'NOT NULL constraint failed: thermometers.path' \
        in str(excinfo.value)


def test_path_not_empty_string(db_session: Session,
                               test_kettle: Kettle) -> None:
    new_thermometer = Thermometer(name='Test thermometer',
                                  path='',
                                  appliance=test_kettle)
    db_session.add(new_thermometer)

    with pytest.raises(IntegrityError) as excinfo:
        db_session.commit()

    assert 'CHECK constraint failed: thermometers' \
        in str(excinfo.value)


def test_not_duplicate_name(db_session: Session,
                            test_thermometer: Thermometer) -> None:
    dup = Thermometer(name=test_thermometer.name,
                      appliance=test_thermometer.appliance,
                      path='Other path')
    db_session.add(dup)

    with pytest.raises(IntegrityError) as excinfo:
        db_session.commit()

    assert ('UNIQUE constraint failed: thermometers.appliance_id, '
            'thermometers.name') in str(excinfo.value)


def test_different_appliances_same_thermometer_name(
        db_session: Session,
        test_thermometer: Thermometer) -> None:
    second_kettle = Kettle(on_tab=TabType.BREW_HOUSE, name='Second')
    db_session.add(second_kettle)
    db_session.commit()

    dup = Thermometer(appliance_id=second_kettle.id,
                      name=test_thermometer.name,
                      path='Other path')
    db_session.add(dup)
    db_session.commit()

    assert dup.id is not None


def test_not_duplicate_path(db_session: Session,
                            test_thermometer: Thermometer) -> None:
    dup = Thermometer(name='Second',
                      appliance=test_thermometer.appliance,
                      path=test_thermometer.path)
    db_session.add(dup)

    with pytest.raises(IntegrityError) as excinfo:
        db_session.commit()

    assert ('UNIQUE constraint failed: thermometers.appliance_id, '
            'thermometers.path') in str(excinfo.value)


def test_different_appliances_same_path(db_session: Session,
                                        test_thermometer: Thermometer) -> None:
    second_kettle = Kettle(on_tab=TabType.BREW_HOUSE, name='Second')
    db_session.add(second_kettle)
    db_session.commit()

    dup = Thermometer(appliance_id=second_kettle.id,
                      name='Other name',
                      path=test_thermometer.path)
    db_session.add(dup)
    db_session.commit()

    assert dup.id is not None


def test_non_existing_appliance(db_session: Session) -> None:
    new_thermometer = Thermometer(appliance_id=-1,
                                  name='Test thermometer',
                                  path='test_path')
    db_session.add(new_thermometer)

    with pytest.raises(IntegrityError) as exc:
        db_session.commit()

    assert 'FOREIGN KEY constraint failed' in str(exc.value)
